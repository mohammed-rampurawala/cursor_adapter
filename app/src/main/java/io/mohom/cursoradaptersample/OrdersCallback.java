package io.mohom.cursoradaptersample;

import android.database.Cursor;

public interface OrdersCallback {
    void onOrdersReceived(Cursor cursor);
}
