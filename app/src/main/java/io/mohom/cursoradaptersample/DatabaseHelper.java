package io.mohom.cursoradaptersample;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import static io.mohom.cursoradaptersample.DatabaseContract.MyOrders.*;

public class DatabaseHelper {
    private static final DatabaseHelper ourInstance = new DatabaseHelper();

    private SampleSqliteOpenHelper sampleSqliteOpenHelper;

    public static DatabaseHelper getInstance() {
        return ourInstance;
    }

    private DatabaseHelper() {
    }

    public void init(Context context) {
        sampleSqliteOpenHelper = new SampleSqliteOpenHelper(context);
    }

    public Cursor getOrders() {
        SQLiteDatabase db = sampleSqliteOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        return cursor;
    }

  /*  public void insertDefaultEntries() {
        for (int i = 0; i < 4; i++) {
            ContentValues values = new ContentValues();
            values.put("order_id", String.valueOf(i));
            values.put("order_status", "order_status" + i);
            sampleSqliteOpenHelper.getWritableDatabase().insert("MY_ORDERS", null, values);
        }
    }*/



}
