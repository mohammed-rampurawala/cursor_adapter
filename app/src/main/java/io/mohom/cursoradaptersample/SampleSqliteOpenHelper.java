package io.mohom.cursoradaptersample;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static io.mohom.cursoradaptersample.DatabaseContract.*;

public class SampleSqliteOpenHelper extends SQLiteOpenHelper {
    String myOrders = "CREATE TABLE MY_ORDERS (_id integer primary key autoincrement, order_id string not null, order_status string not null )";

    public SampleSqliteOpenHelper(Context context) {
        super(context, "sample", null, BuildConfig.DB_VERSION);
        System.out.println("DatabaseSample constructor called");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("DatabaseSample creation");
        db.execSQL(myOrders);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("DatabaseSample Old Version::"+oldVersion+" New Versiont::"+newVersion);
        db.execSQL("ALTER TABLE " + MyOrders.TABLE_NAME + "  ADD " + MyOrders.ORDER_DATE + " long");
    }
}
