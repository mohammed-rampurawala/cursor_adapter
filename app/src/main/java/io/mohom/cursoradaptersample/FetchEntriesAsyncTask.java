package io.mohom.cursoradaptersample;

import android.database.Cursor;
import android.os.AsyncTask;

public class FetchEntriesAsyncTask extends AsyncTask<Void, Void, Cursor> {

    private final OrdersCallback orderCallback;

    public FetchEntriesAsyncTask(OrdersCallback callback){
        this.orderCallback = callback;
    }

    @Override
    protected Cursor doInBackground(Void... voids) {
        Cursor orders = DatabaseHelper.getInstance().getOrders();
        return orders;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        super.onPostExecute(cursor);
        if(orderCallback!=null){
            orderCallback.onOrdersReceived(cursor);
        }
    }
}
