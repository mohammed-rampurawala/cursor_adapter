package io.mohom.cursoradaptersample;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements OrdersCallback {

    private ListView ordersListView;
    private MyCursorAdapter myCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ordersListView = (ListView) findViewById(R.id.orders_listview);
        myCursorAdapter = new MyCursorAdapter(getApplicationContext(), null, false);
        ordersListView.setAdapter(myCursorAdapter);
        new FetchEntriesAsyncTask(this).execute();
    }

    @Override
    public void onOrdersReceived(Cursor cursor) {
        System.out.println("Cursor Received::" + cursor.getCount());
        myCursorAdapter.changeCursor(cursor);
    }

    class MyCursorAdapter extends CursorAdapter {
        int orderStatusIndex = -1;
        public MyCursorAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
            if(c!=null){
                orderStatusIndex = c.getColumnIndex(DatabaseContract.MyOrders.ORDER_STATUS);
            }
        }

        @Override
        public void changeCursor(Cursor cursor) {
            super.changeCursor(cursor);
            if(cursor!=null){
                orderStatusIndex = cursor.getColumnIndex(DatabaseContract.MyOrders.ORDER_STATUS);
            }
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return getLayoutInflater().inflate(R.layout.order_item, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView orderStatus = view.findViewById(R.id.order_status_tv);
            orderStatus.setText(cursor.getString(orderStatusIndex));
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public long getItemId(int position) {
            Cursor cursor = getCursor();
            boolean moved = cursor.moveToPosition(position);
            if (!moved) {
                return position;
            } else {
                return cursor.getLong(cursor.getColumnIndex(DatabaseContract.MyOrders._ID));
            }
        }
    }
}
