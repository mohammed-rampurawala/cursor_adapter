package io.mohom.cursoradaptersample;

import android.provider.BaseColumns;

public final class DatabaseContract {
    public interface MyOrders extends BaseColumns{
        public String TABLE_NAME="my_orders";
        public String ORDER_STATUS="order_status";
        public String ORDER_ID="order_id";
        public String ORDER_DATE="order_date";
    }
}
