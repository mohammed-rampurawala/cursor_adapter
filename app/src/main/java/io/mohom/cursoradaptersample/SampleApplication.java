package io.mohom.cursoradaptersample;

import android.app.Application;

public class SampleApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseHelper helper = DatabaseHelper.getInstance();
        helper.init(getApplicationContext());
//        helper.insertDefaultEntries();
    }
}
